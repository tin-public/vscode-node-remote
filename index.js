const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 80;

mongoose.connect('mongodb://mongo:27017/db', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.get('/', (req, res) => res.send(`DB readyState: ${mongoose.connection.readyState}`));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
